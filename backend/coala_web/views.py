import json

from brake.decorators import ratelimit

from django.core.management import call_command
from django.http import HttpResponse
from django.views.decorators.csrf import csrf_exempt
from django.views.decorators.http import require_http_methods


@ratelimit(block=True, rate='50/m')
@csrf_exempt
@require_http_methods(['GET'])
def dump_database(request):
    call_command('migrate', '--run-syncdb')
    call_command('dumpdata', output='data.json')
    with open('data.json') as f:
        data = json.load(f)
    return HttpResponse(json.dumps(data), content_type='application/json')
