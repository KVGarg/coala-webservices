from functools import lru_cache
import datetime

from org.models import Contributor
from igitt_django.models import IGittIssue, IGittMergeRequest

NEWCOMERS_TEAM_NAME = 'coala newcomers'
REMOVE_NEWCOMERS = ['co-robo']
LAST_ACTIVE = (
    datetime.datetime.today() - datetime.timedelta(
        3*365/12)).isoformat()


@lru_cache(maxsize=32)
def all_newcomers():
    """
    Get newcomers all the newcomers.

    :return: the list of newcomer usernames
    """
    newcomers_list = []
    contributors = Contributor.objects.all()
    for contributor in contributors:
        # Get newcomers who belongs to only one team
        # and that is newcomers team.
        contributor_teams = contributor.teams.all()
        if contributor_teams.count() == 1:
            if contributor_teams[0].name == NEWCOMERS_TEAM_NAME:
                newcomers_list.append(contributor.login)
    for newcomer in REMOVE_NEWCOMERS:
        if newcomer in newcomers_list:
            newcomers_list.remove(newcomer)
    return newcomers_list


def active_newcomers():
    """
    Get the newcomers who are active in past three months.
    """
    active_newcomers_list = []
    issues = IGittIssue.objects.all()
    mrs = IGittMergeRequest.objects.all()
    all_newcomers_list = all_newcomers()
    # Get the datetime three months ago from today.
    for issue in issues:
        i_updated_at = issue.data['updated_at']
        if i_updated_at <= LAST_ACTIVE:
            continue
        author = issue.data['author']
        if author in all_newcomers_list:
            if author not in active_newcomers_list:
                active_newcomers_list.append(author)
    for mr in mrs:
        m_updated_at = mr.data['updated_at']
        if m_updated_at <= LAST_ACTIVE:
            continue
        author = issue.data['author']
        if author in all_newcomers_list:
            if author not in active_newcomers_list:
                active_newcomers_list.append(author)
    return active_newcomers_list
