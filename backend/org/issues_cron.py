import uuid
from functools import lru_cache

from org.git import get_igitt_org
from igitt_django.models import IGittIssue, IGittRepository


@lru_cache(maxsize=32)
def get_issues(hoster):
    data = dict()
    repo_dict = dict()
    org = get_igitt_org(hoster)
    repos = org.repositories
    for repo in repos:
        repo_full_name = repo.full_name
        repo_id = repo.identifier
        repo_dict[repo.identifier] = {
            'id': repo_id,
            'full_name': repo_full_name,
            'hoster': hoster,
        }
        issues = repo.filter_issues(state='all')
        for issue in issues:
            id = uuid.uuid4().int & (1 << 32)-1
            number = issue.number
            url = (
                'https://' + hoster + '.com/' + repo_full_name +
                '/issues/' + number)
            data[id] = {
                'number': number,
                'title': issue.title,
                'description': issue.description,
                'url': url,
                'author': issue.author.username,
                'created_at': issue.created,
                'updated_at': issue.updated,
                'state': issue.state.value,
                'repo': repo.full_name,
                'repo_id': repo_id,
                'reactions': [r.name for r in issue.reactions],
                'assignees': [
                    assignee.username for assignee in issue.assignees],
                'labels': [label for label in issue.labels],
                'mrs_closed_by': [int(i.number) for i in issue.mrs_closed_by],
                'hoster': hoster,
            }

    # Saving dict to database
    for repo in repo_dict:
        if IGittRepository.objects.filter(id=repo).exists():
            r = IGittRepository.objects.get(id=repo)
        else:
            r = IGittRepository()
            r.id = repo
        r.full_name = repo_dict[repo]['full_name']
        r.hoster = repo_dict[repo]['hoster']
        r.save()

    for issue in data:
        number = data[issue]['number']
        repo_id = data[issue]['repo_id']
        if IGittIssue.objects.filter(number=number, repo_id=repo_id).exists():
            i = IGittIssue.objects.get(number=number, repo_id=repo_id)
        else:
            i = IGittIssue()
            i.id = issue
        i.number = data[issue]['number']
        i.repo_id = data[issue]['repo_id']
        i.data = data[issue]
        i.save()
