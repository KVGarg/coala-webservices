import os
import requests

import json
import logging

from django.core.management import call_command
from django.core.management.base import BaseCommand

from coala_web.settings import ORG_NAME


class Command(BaseCommand):
    help = 'Fetch the deployed data and add them back.'

    def handle(self, *args, **options):
        webservices_url = 'https://webservices.{org}.io'.format(org=ORG_NAME)
        dump_db_url = '{url}/dump/database'.format(url=webservices_url)
        deployed_database_response = requests.get(dump_db_url)
        response_headers = deployed_database_response.headers
        if response_headers['Content-Type'] == 'application/json':
            db_json_file = os.path.join(os.getcwd(), 'data.json')
            with open(db_json_file, 'w+') as f:
                json.dump(deployed_database_response.json(), f, indent=4)
            call_command('sqlflush')
            call_command('loaddata', db_json_file)
        else:
            content_type = response_headers['Content-type']
            message = ("URL {url} doesn't exist. Returned {type}"
                       ' Content-Type as response'.format(url=dump_db_url,
                                                          type=content_type))
            logging.error(message)
